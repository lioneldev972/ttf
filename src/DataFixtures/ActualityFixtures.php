<?php

namespace App\DataFixtures;

use App\Entity\Actuality;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ActualityFixtures extends Fixture
{
    
    public const NUMBERS_OF_ACTUALITY = 10;

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for($i = 1; $i <= self::NUMBERS_OF_ACTUALITY; $i++) {
            $actuality = new Actuality();
            $tailleSentence = mt_rand(2, 6);
            $ladate = $this->randomDate();
            $actuality->setTitle($faker->sentence(5, true))
                ->setContent($faker->paragraph($this->randomNumber(),false))
                ->setDateActu($faker->dateTime($ladate))
                ->setImage('https://via.placeholder.com/350x200');

            $manager->persist($actuality);

        }
        $manager->flush();
    }
    private function randomNumber($min = 2, $max = 6): int
    {
        return mt_rand($min, $max);
    }

    private function randomDate(){

        //hier 
        $end = strtotime("-1 day");
        //date antérieur a max 3 mois 
        $start = strtotime("-2 month");        
        //on choisi une date aléatoire comprise entre hier et 2 mois 
        $timestamp = mt_rand($start, $end);
        //Print it out.
        $theDate = date("Y-m-d h:i:s", $timestamp);        
        return $theDate;
    }
}
