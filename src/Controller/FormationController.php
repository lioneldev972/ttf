<?php

namespace App\Controller;

use App\Entity\Formation;
use App\Hydrator\CompanyHydrator;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request; // Nous avons besoin d'accéder à la requête pour obtenir le numéro de page
use Knp\Component\Pager\PaginatorInterface; // Nous appelons le bundle KNP Paginator

class FormationController extends AbstractController
{
    /**
     * @Route("/formation/{id}", name="formation")
     * @param CompanyHydrator $companyHydrator
     * @param $id
     * @return Response
     */
    public function index(CompanyHydrator $companyHydrator,$id, EntityManagerInterface $entityManager): Response
    {
        $formationRepository = $entityManager->getRepository(Formation::class);

        $myFormation = $formationRepository->find($id);

        $companyHydrator->hydrateCollection([$myFormation]);
        return $this->render('formation/index.html.twig', [
            'formation' => $myFormation
        ]);
    }


    /**
     * @Route("/formations", name="listing_formation")
     * @param CompanyHydrator $companyHydrator
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function listeFormation(CompanyHydrator $companyHydrator,Request $request, PaginatorInterface $paginator, EntityManagerInterface $entityManager): Response
    {
        $formationRepository = $entityManager->getRepository(Formation::class);
        $resultFormation = $formationRepository->findLatestFormations();

        $listOfFormations = $paginator->paginate(
            $resultFormation, // Requête contenant les données à paginer (ici nos articles)
            $pageCurrent = $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            9 // Nombre de résultats par page
        );

        $companyHydrator->hydrateCollection((array)$listOfFormations->getItems());

        $listOfFormations->setPageRange(3);
        
        return $this->render('formation/listing.html.twig', [
            'formationList' => $listOfFormations,
            'pageCurrent' => $pageCurrent
        ]);
    }
}
