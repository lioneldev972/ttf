<?php

namespace App\Controller;

use App\Entity\Actuality;
use App\Hydrator\CompanyHydrator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request; // Nous avons besoin d'accéder à la requête pour obtenir le numéro de page
use Knp\Component\Pager\PaginatorInterface; // Nous appelons le bundle KNP Paginator


class ActualityController extends AbstractController
{
    /**
     * @Route("/actuality/{id}", name="actuality")
     * @param Actuality $actuality
     * @return Response
     */
    public function index(Actuality $actuality): Response
    {
        return $this->render('actuality/index.html.twig', [
            'actuality' => $actuality
        ]);
    }

    /**
     * @Route("/actuality", name="listing_actuality")
     * @param CompanyHydrator $companyHydrator
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function listeActuality(CompanyHydrator $companyHydrator,Request $request, PaginatorInterface $paginator, EntityManagerInterface $entityManager): Response
    {
        $actualityRepository = $entityManager->getRepository(Actuality::class);
        $resultactuality = $actualityRepository->findLatestActuaclityDate();

        $listOfActualities = $paginator->paginate(
            $resultactuality, // Requête contenant les données à paginer (ici nos articles)
            $pageCurrent = $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            10 // Nombre de résultats par page
        );

       // $companyHydrator->hydrateCollection($listOfActualitys->getItems());

        $listOfActualities->setPageRange(2);
        
        return $this->render('actuality/listing.html.twig', [
            'actualitiesList' => $listOfActualities,
            'pageCurrent' => $pageCurrent
        ]);
    }
}
