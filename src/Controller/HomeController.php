<?php

namespace App\Controller;

use App\Entity\Formation;
use App\Entity\Actuality;
use App\Hydrator\CompanyHydrator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @param CompanyHydrator $companyHydrator
     * @return Response
     */
    public function index(CompanyHydrator $companyHydrator, EntityManagerInterface $entityManager ): Response
    {
        $formationRepository = $entityManager->getRepository(Formation::class);
        $latestFormations = $formationRepository->findLatestFormations(6);

        $companyHydrator->hydrateCollection($latestFormations);


        $ActualityRepository = $entityManager->getRepository(Actuality::class);
        $actulityList = $ActualityRepository->findLatestActuaclityDate(4);


        return $this->render('home/index.html.twig', [
            'formationList' => $latestFormations,
            'actulityList' => $actulityList,
        ]);
    }
}
