<?php

namespace App\Repository;

use App\Entity\Company;
use App\Entity\Formation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
/**
 * @method Formation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Formation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Formation[]    findAll()
 * @method Formation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Formation::class);
    }
    public function findLatestFormation(int $value) {

        return $this->createQueryBuilder('f')

            ->innerJoin(
                Company::class,    // Entity
                'c',               // Alias
                'WITH',        // Join type
                'f.idcompany = c.id' // Join columns
            )
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Formation[] Returns an array of Formation objects
    //  */
    public function findLatestFormations(int $limit = null)
    {
        return $this->createQueryBuilder('f')

            ->innerJoin(
                    Company::class,    // Entity
                    'c',               // Alias
                'WITH',        // Join type
                    'f.idcompany = c.id' // Join columns
                )
            ->orderBy('f.id', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?Formation
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
