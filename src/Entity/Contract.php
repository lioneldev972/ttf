<?php

namespace App\Entity;

use App\Repository\ContractRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ContractRepository::class)
 */
class Contract
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $namecontact;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNamecontact(): ?string
    {
        return $this->namecontact;
    }

    public function setNamecontact(string $namecontact): self
    {
        $this->namecontact = $namecontact;

        return $this;
    }
}
