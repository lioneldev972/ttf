<?php

namespace App\Entity;

use App\Repository\MetierRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MetierRepository::class)
 */
class Metier
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $namemetier;

    /**
     * @ORM\ManyToOne(targetEntity=secteur::class, inversedBy="metiers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idSecteur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNamemetier(): ?string
    {
        return $this->namemetier;
    }

    public function setNamemetier(string $namemetier): self
    {
        $this->namemetier = $namemetier;

        return $this;
    }

    public function getIdSecteur(): ?secteur
    {
        return $this->idSecteur;
    }

    public function setIdSecteur(?secteur $idSecteur): self
    {
        $this->idSecteur = $idSecteur;

        return $this;
    }
}
