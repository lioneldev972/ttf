<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230311233707 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE contract (id INT AUTO_INCREMENT NOT NULL, namecontact VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE metier (id INT AUTO_INCREMENT NOT NULL, id_secteur_id INT NOT NULL, namemetier VARCHAR(255) NOT NULL, INDEX IDX_51A00D8C3AF4C4D2 (id_secteur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE secteur (id INT AUTO_INCREMENT NOT NULL, namesecteur VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE metier ADD CONSTRAINT FK_51A00D8C3AF4C4D2 FOREIGN KEY (id_secteur_id) REFERENCES secteur (id)');
        $this->addSql('DROP TABLE migration_versions');
        $this->addSql('ALTER TABLE company ADD image VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE formation DROP FOREIGN KEY FK_404021BF979B1AD6');
        $this->addSql('DROP INDEX IDX_404021BF979B1AD6 ON formation');
        $this->addSql('ALTER TABLE formation ADD content LONGTEXT DEFAULT NULL, DROP image, CHANGE company_id idcompany_id INT NOT NULL');
        $this->addSql('ALTER TABLE formation ADD CONSTRAINT FK_404021BF5A3207EC FOREIGN KEY (idcompany_id) REFERENCES company (id)');
        $this->addSql('CREATE INDEX IDX_404021BF5A3207EC ON formation (idcompany_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE migration_versions (version VARCHAR(14) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, executed_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(version)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE metier DROP FOREIGN KEY FK_51A00D8C3AF4C4D2');
        $this->addSql('DROP TABLE contract');
        $this->addSql('DROP TABLE metier');
        $this->addSql('DROP TABLE secteur');
        $this->addSql('ALTER TABLE company DROP image');
        $this->addSql('ALTER TABLE formation DROP FOREIGN KEY FK_404021BF5A3207EC');
        $this->addSql('DROP INDEX IDX_404021BF5A3207EC ON formation');
        $this->addSql('ALTER TABLE formation ADD image VARCHAR(255) NOT NULL, DROP content, CHANGE idcompany_id company_id INT NOT NULL');
        $this->addSql('ALTER TABLE formation ADD CONSTRAINT FK_404021BF979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('CREATE INDEX IDX_404021BF979B1AD6 ON formation (company_id)');
    }
}
